<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class VerifyLogin extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('useremail', 'User email', 'trim|required');
        $this->form_validation->set_rules('userpass', 'Password', 'trim|required|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->load->view('index_login');
        } else {
            //Go to private area
            redirect('Welcome', 'refresh');
        }
    }

    function check_database($password) {
        //Field validation succeeded.  Validate against database
        $useremail = $this->input->post('useremail');

        //query the database
        $result = $this->Usermodel->login($useremail, $password);
        if ($result) {
            $sess_array = array();
            foreach ($result as $row) {
                $sess_array = array(
                    'memberId' => $row->memberId,
                    'memberEmail' => $row->memberEmail
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid User email or password');
            return false;
        }
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function index() {
        $data = array(
            'email' => "",
            'password' => ""
        );
        $this->load->view('index_login', $data);
    }

    function signup() {
        $this->load->view('index_signup');
    }

    function authed() {
        $this->load->view('authed/home_view');
    }

    function addUser(){
        
        //get  form values
        $username = $_POST['username'];
        $useremail = $_POST['useremail'];
        $userreg = $_POST['userreg'];
        $userpass = $_POST['userpass'];
        
        $userInfo = array(
            'userName' => $username,
            'memberEmail' => $useremail,
            'memberRegistration' => $userreg,
            'memberPassword' => md5($userpass)
        );
        
        //insert
        $queryInsert = $this->db->insert('MEMBER', $userInfo);
        
        if($queryInsert){
            $loginInfo = array(
                'email' => $useremail,
                'password' => $userpass
            );
            
            $this->load->view('index_login', $loginInfo);
        }
    }
}

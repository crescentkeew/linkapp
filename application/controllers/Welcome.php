<?php

class Welcome extends CI_Controller {

    function index() {
        if ($this->session->userdata('logged_in')) {
            $session_data = $this->session->userdata('logged_in');
            $data['memberEmail'] = $session_data['memberEmail'];
            $this->load->view('authed/home_view', $data);
        } else {
            //If no session, redirect to login page
            redirect('sign_in', 'refresh');
        }
    }

    function logout() {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('sign_in', 'refresh');
    }

}

<?php

Class Usermodel extends CI_Model {

    function login($useremail, $password) {
        $this->db->select('memberId, memberEmail, memberPassword');
        $this->db->from('MEMBER');
        $this->db->where('memberEmail', $useremail);
        $this->db->where('memberPassword', MD5($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

}

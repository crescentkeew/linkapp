<!DOCKTYPE html>

<html>
    <head>
        <title>Linkapp | Connect with People</title>

        <!--Css-->
        <link href="<?php echo base_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/linkapp.min.css"); ?>" rel="stylesheet">

    </head>
    <body>

        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#user" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo base_url(); ?>" class="navbar-brand">Linkapp</a>
                </div>

                <div class="collapse navbar-collapse" id="user">
                    <form class="navbar-form navbar-left" method="get" action="?search">
                        <div class="form-group">
                            <input class="form-control input-search" type="text" name="searchval" id="searchval" placeholder="Search academics entertainment intrests ...."/>
                        </div>
                        <input type="submit" class="hidden" value="Search"/>
                    </form>
                    <ul class="navbar-nav navbar-right nav">
                        <li>
                            <a>Academics</a>
                        </li>
                        <li>
                            <a>Entertainment</a>
                        </li>
                        <li>
                            <a>Sports</a>
                        </li>
                        <li>
                            <a>User profile</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!--Js-->
        <script src="<?php echo base_url("assets/js/jquery-1.11.0.min.js");?>"></script>
        <script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
    </body>
</html>
<!DOCKTYPE html>

<html>
    <head>
        <title>Linkapp</title>
        
        <!--Css-->
        <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/linkapp.min.css");?>" rel="stylesheet">
        
    </head>
    <body>
        
        <div class="container">
            <div class="col-md-4 col-md-offset-4">
                <div id="signup">
                    <h2>Create a member account</h2>
                    
                    <form method="post" action="<?php echo base_url("add_member");?>">
                        <div class="form-group">
                            <label class="control-label">Username</label>
                            <input type="text" class="form-control" name="username" id="username" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="useremail" id="useremail" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Registration number</label>
                            <input type="text" class="form-control" name="userreg" id="userreg" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password</label>
                            <input type="password" class="form-control" name="userpass" id="userpass" placeholder="" required="">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-sm form-control" value="Create account">
                        </div>
                    </form>
                    
                    <div class="text-center"> Already a member? <a href="<?php echo base_url("sign_in");?>">Sign in</a></div>
                </div>
            </div>
        </div>
        
        <!--Js-->
        
    </body>
</html>